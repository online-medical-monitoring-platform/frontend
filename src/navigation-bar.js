import React from 'react'
import logo from './commons/images/icon.png';
import {
    Button
} from 'reactstrap';

import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';
import {Redirect} from "react-router-dom";

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

class NavigationBar extends React.Component {

    constructor(props) {
        super(props);
        this.logoutHandle = this.logoutHandle.bind(this);

        this.state = {
            redirect:false,
            selectedLogout: false,
        }
    }

    reloadLogout(){
        this.setState({reloadLogout: !this.state.reloadLogout});
    }

    logoutHandle(){
        localStorage.clear();
        this.setState({
            redirect:true
        })
    }


    render() {
        if(this.state.redirect)
        {
            return <Redirect to={'/login'}/>;
        }
        return (
    <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"}
                     height={"35"} />
            </NavbarBrand>
            <Nav className="mr-auto" navbar>

                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle style={textStyle} nav caret>
                       Menu
                    </DropdownToggle>
                    <DropdownMenu right >

                        <DropdownItem>
                            <NavLink href="/doctor/patient">Patients</NavLink>
                        </DropdownItem>
                        <DropdownItem>
                            <NavLink href="/doctor/caregiver">Caregivers</NavLink>
                        </DropdownItem>
                        <DropdownItem>
                            <NavLink href="/doctor/medication">Medications</NavLink>
                        </DropdownItem>
                        <DropdownItem>
                            <NavLink href="/doctor/medicationPlan">Medication plan</NavLink>
                        </DropdownItem>

                    </DropdownMenu>
                </UncontrolledDropdown>
                <Button onClick={
                  /* localStorage.setItem("role","");
                    localStorage.setItem("username","");
                    localStorage.setItem("password","");
                    localStorage.setItem("id","");
                    this.reloadLogout();*/
                    this.logoutHandle
                }>Logout </Button>

            </Nav>
        </Navbar>
    </div>
)
     }}

export default NavigationBar
