import React, { Component } from 'react';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import MedicationUpdateForm from "./medication-update-form";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import * as API_MEDICATIONS from "../api/medication-api";


const filters = [
    {
        accessor: 'name',
    }
];

class CaregiverTable extends Component {

    constructor(props) {
        super(props);
        this.state = {
            update: false,
            selectedMedication: '',
            medications : this.props.medications,

        }
        this.toggleUpdateForm = this.toggleUpdateForm.bind(this);
        this.updateMedication = this.updateMedication.bind(this);
    }

    toggleUpdateForm() {

        this.setState({
            update: !this.state.update
        });


    }

    deleteMedication(id) {
        return API_MEDICATIONS.deleteMedication(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted medication with id: " + result);
                this.props.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }

    updateMedication(id) {
        const index = this.state.medications.findIndex(medications => {
            return medications.id === id
        });
        this.setState({
            selectedMedication: this.state.medications[index],
            update: !this.state.update
        });
        console.log(this.state.medications[index]);
        this.toggleUpdateForm();
    }


    render() {
        const data = this.state.medications;
        const columns = [
            {
                Header: "Name",
                accessor: "name",
                style:{
                    textAlign: "center"
                },
                width: 270
            },
            {
                Header: "Side effects",
                accessor: "sideEffects",
                style:{
                    textAlign: "center"
                },
                width: 450
            },
            {
                Header: "Dosage",
                accessor: "dosage",
                style:{
                    textAlign: "center"
                },
                width: 140
            },
            {
                Header: "Actions",
                filterable: false,
                sortable: false,
                resizable: false,
                Cell: medications =>{
                    return(
                        <div>
                            <button style={{background: "red", color: "#fefefe",float:"left"}}
                                    onClick={(e)=> {
                                        this.deleteMedication(medications.original.id);
                                    }}
                            >Delete</button>
                            <button style={{background: "blue", color: "#fefefe" ,float:"right"}}
                                    onClick={(e)=> {
                                        this.updateMedication(medications.original.id);
                                    }}
                            >Update</button>
                        </div>
                    )},
                width: 150,
                maxWidth: 150,
                minWidth: 150,
                style:{
                    textAlign: "center"
                },
            }
        ]
        return (
            <div>
                <ReactTable
                    className="-striped -highlight"
                    data={data}
                    search={filters}
                    columns={columns}
                    defaultPageSize={10}
                >
                </ReactTable>
                <Modal isOpen={this.state.update} toggle={this.toggleUpdateForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleUpdateForm}> Update Medication: </ModalHeader>
                    <ModalBody>
                        <MedicationUpdateForm medication={this.state.selectedMedication}  reloadHandlerUpdate={this.props.reloadHandler}/>
                    </ModalBody>
                </Modal>
            </div>


        );
    }
}

export default CaregiverTable;
