import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import MedicationForm from "../medication/components/medication-form";

import * as API_MEDICATIONS from"../medication/api/medication-api"
import MedicationTable from "./components/medication-table";
import NavigationBar from "../navigation-bar";



class MedicationContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.reloadUpdate = this.reloadUpdate.bind(this);

        this.state = {
            delete: false,
            id: '',
            selected: false,
            collapseForm: false,
            collapseFormUpdate:false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        }

    }

    componentDidMount() {
        this.fetchMedication();
    }

    fetchMedication(){
        return API_MEDICATIONS.getMedications((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }



    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchMedication();
    }
    reloadUpdate() {
        this.setState({
            isLoaded: false
        });
        this.fetchMedication();
    }

    render() {
        return (
            <div>
                <NavigationBar />
                <CardHeader>
                    <strong> Medication Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Medication </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 2}}>
                            {this.state.isLoaded && <MedicationTable medications = {this.state.tableData}  reloadHandler={this.reloadUpdate}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>
                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Medication: </ModalHeader>
                    <ModalBody>
                        <MedicationForm reloadHandler={this.reload} />
                    </ModalBody>
                </Modal>

            </div>
        )
    }
}


export default MedicationContainer;
