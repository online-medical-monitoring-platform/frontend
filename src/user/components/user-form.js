import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import {  MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import * as API_USERS from "../api/user-api";


const theme = createMuiTheme();
const delay = () => new Promise(resolve => setTimeout(() => resolve("data fetched"), 4000));



export class UserForm extends Component {
    //_isMounted = false;

    constructor(props) {
        super(props);
        this.registerUser = this.registerUser.bind(this);
        this.handleChange = this.handleChange.bind(this);
       // this.continue = this.continue.bind(this);

        this.reloadHandler = this.props.reloadHandler;
        this.state = {
            username:"",
            password:"",
            userId:""
        }
}

    registerUser(user){
         return API_USERS.postUser(user, (result, status, error) => {
             if (result !== null && (status === 200 || status === 201)) {
                 console.log("Successfully inserted user with id: " + result);
                 this.setState({userId: result});
             } else {
                 this.setState(({
                     errorStatus: status,
                     error: error
                 }));
             }
             //new Promise(resolve => setTimeout(() => resolve("data fetched"), 4000));
         });
    }


    continue = e => {
        e.preventDefault();
        if(this.state.username != "" && this.state.password!=""){
            let user = {
                username: this.state.username,
                password: this.state.password
            };
            this.registerUser(user).then(r => this.props.setUserId(this.state.userId) );
        }
        this.props.nextStep();
    };

 /*   componentDidMount() {
        this._isMounted = true;

        callAPI_or_DB(...).then(result => {
            if (this._isMounted) {
                this.setState({isLoading: false})
            }
        });
    }

    componentWillUnmount() {
        //this.props.setUserId(this.state.userId);
        this._isMounted = false;
    }*/

    handleChange = input => e => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value});
    };

    render() {
        const { values, handleChange } = this.props;
        return (

                    <div>
                        <h1>Create account</h1>
                        <TextField
                            placeholder="Enter pacient's username"
                            label="Username"
                            name="username"
                            onBlur={this.handleChange('username')}
                            defaultValue={values.username}
                            margin="normal"
                            fullWidth
                        />
                        <br />
                        <TextField
                            placeholder="Enter pacient's password"
                            label="Password"
                            name="password"
                            onBlur={this.handleChange('password')}
                            defaultValue={values.password}
                            margin="normal"
                            fullWidth
                            type="password"
                        />
                        <br />


                        <Button
                            color="primary"
                            variant="contained"
                            onClick={this.continue}
                        >Continue</Button>
                    </div>
        );
    }
}

export default UserForm;
