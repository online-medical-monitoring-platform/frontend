import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import CaregiverForm from "../caregiver/components/caregiver-form";

import * as API_CAREGIVERS from"../caregiver/api/caregiver-api"
import CaregiverTable from "./components/caregiver-table";
import NavigationBar from "../navigation-bar";


class CaregiverContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.reloadUpdate = this.reloadUpdate.bind(this);

        this.state = {
            delete: false,
            id: '',
            selected: false,
            collapseForm: false,
            collapseFormUpdate:false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        }

    }

    componentDidMount() {
        this.fetchCaregivers();
    }

    fetchCaregivers(){
        return API_CAREGIVERS.getCaregivers((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }



    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchCaregivers();
    }
    reloadUpdate() {
        this.setState({
            isLoaded: false
        });
        this.fetchCaregivers();
    }



    render() {

        return (
            <div>
                <NavigationBar />
                <CardHeader>
                    <strong> Caregiver Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Caregiver </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '10', offset: 1}}>
                            {this.state.isLoaded && <CaregiverTable caregivers = {this.state.tableData}  reloadHandler={this.reloadUpdate}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>
                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Caregiver: </ModalHeader>
                    <ModalBody>
                        <CaregiverForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

            </div>
        )
    }
}


export default CaregiverContainer;
