import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Card,
    CardHeader
} from 'reactstrap';
import * as API_CAREGIVERS from"../caregiver/api/caregiver-api"
import CaregiverDetails from "./components/caregiver-details";
import {Redirect} from "react-router-dom";
import {Client} from "@stomp/stompjs";
import {Container} from 'reactstrap';





class CaregiverPage extends React.Component {

    constructor(props) {
        super(props);
        this.reload = this.reload.bind(this);
        this.logoutHandle = this.logoutHandle.bind(this);

        this.state = {
            ws:null,
            message:'',

            redirect:false,

            delete: false,
            id: Number(localStorage.getItem('id')),
            selected: false,
            collapseForm: false,
            collapseFormUpdate:false,


            patients: [],
            isLoaded: false,
            isLoadedPatients: false,
            caregiver:null,
            errorStatus: 0,
            error: null
        }

    }

    logoutHandle(){
        localStorage.clear();
        this.setState({
            redirect:true
        })
    }


    componentDidMount() {
        this.fetchPatients();
        this.fetchCaregiver();

        this.client = new Client();
        this.client.configure({
            brokerURL: 'wss://serban-codruta-sd-back.herokuapp.com/socket',
            //brokerURL: 'ws://localhost:8080/socket',
            onConnect: () => {
                console.log('Connected to websocket');
                this.client.subscribe('/topic/message', message => {
                    console.log("Mesaje "+this.state.message)
                    this.setState({
                        message: this.state.message + ' ' + message.body + '\r\n'
                    });
                });
            },
        });

        this.client.activate();
    }

    fetchCaregiver() {
        return API_CAREGIVERS.getCaregiverById(Number(localStorage.getItem('id')),(result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    caregiver: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
          // console.log("Din fetch"+this.state.caregiver.name);

        });
    }

    fetchPatients() {
        return API_CAREGIVERS.getPatients(Number(localStorage.getItem('id')),(result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    patients: result,
                    isLoadedPatients: true
                })
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    reload() {
        if(this.state.isLoadedPlans) {
            this.setState({
                isLoaded: false,
                isLoadedPlans: false
            });
            this.fetchMedicationPlans();
            this.fetchPatient();
        }
    }



    render() {
          /*console.log("in render in page");
           console.log(this.state.caregiver);
           console.log("in render in page patient[0]");
           console.log(this.state.patients);*/
        if(this.state.redirect)
        {
            return <Redirect to={'/login'}/>;
        }
        return (
            <div>
                <CardHeader>
                    <strong style={{fontSize:'30px'}}> Caregiver  </strong>
                    <button style={{marginLeft:'20px'}} onClick={this.logoutHandle} >Log out</button>

                </CardHeader>
                <Card >
                    {this.state.isLoadedPatients && this.state.isLoaded &&  <CaregiverDetails  caregiver={this.state.caregiver} patients={this.state.patients}/>}
                    {this.state.errorStatus > 0 && <APIResponseErrorMessage
                        errorStatus={this.state.errorStatus}
                        error={this.state.error}
                    />   }

                </Card>

                <Card>
                    {this.state.message &&
                    <Container fluid>
                        <h3 style={{marginLeft:'40px'}}> Patient problem</h3>
                        <p className="lead" style={{marginLeft:'30px', marginBottom:'15ps'}} >
                            {this.state.message.toString()}</p>
                    </Container>}
                </Card>
            </div>
        )

    }
}


export default CaregiverPage;
