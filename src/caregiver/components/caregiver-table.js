import React, { Component } from 'react';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import CaregiverUpdateForm from "./caregiver-update-form";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import * as API_CAREGIVERS from "../api/caregiver-api";


const filters = [
    {
        accessor: 'name',
    }
];

class CaregiverTable extends Component {

    constructor(props) {
        super(props);
        this.state = {
            update: false,
            selectedCaregiver: '',
            caregivers : this.props.caregivers,

        }
        this.toggleUpdateForm = this.toggleUpdateForm.bind(this);
        this.updateCaregiver = this.updateCaregiver.bind(this);
    }

    toggleUpdateForm() {

        this.setState({
            update: !this.state.update
        });


    }

    deleteCaregiver(id) {
        return API_CAREGIVERS.deleteCaregiver(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted caregiver with id: " + result);
                this.props.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }

    updateCaregiver(id) {
        console.log("caregivers:"+this.props.caregivers.size);
        const index = this.state.caregivers.findIndex(caregivers => {
            return caregivers.id === id
        });
        this.setState({
            selectedCaregiver: this.state.caregivers[index],
            update: !this.state.update
        });
        console.log(this.state.caregivers[index]);
        this.toggleUpdateForm();
    }


    render() {
        const data = this.state.caregivers;
        const columns = [
            {
                Header: "Name",
                accessor: "name",
                style:{
                    textAlign: "center"
                },
                width: 250
            },
            {
                Header: "Birthdate",
                accessor: "birthdate",
                style:{
                    textAlign: "center"
                },
                width: 150
            },
            {
                Header: "Gender",
                accessor: "gender",
                style:{
                    textAlign: "center"
                },
                width: 100
            },
            {
                Header: "Address",
                accessor: "address",style:{
                    textAlign: "center"
                },
                width: 300
            },
            {
                Header: "Username",
                accessor: "username",
                style:{
                    textAlign: "center"
                },
                width: 160
            },{
                Header: "Password",
                accessor: "password",
                style:{
                    textAlign: "center"
                },
                width: 160
            },
            {
                Header: "Actions",
                filterable: false,
                sortable: false,
                resizable: false,
                Cell: caregivers =>{
                    return(
                        <div>
                            <button style={{background: "red", color: "#fefefe",float:"left"}}
                                    onClick={(e)=> {
                                        this.deleteCaregiver(caregivers.original.id);
                                    }}
                            >Delete</button>
                            <button style={{background: "blue", color: "#fefefe" ,float:"right"}}
                                    onClick={(e)=> {
                                        this.updateCaregiver(caregivers.original.id);
                                    }}
                            >Update</button>
                        </div>
                    )},
                width: 150,
                maxWidth: 150,
                minWidth: 150,
                style:{
                    textAlign: "center"
                },
            }
        ]
        return (
            <div>
                <ReactTable
                    className="-striped -highlight"
                    data={data}
                    search={filters}
                    columns={columns}
                    defaultPageSize={10}
                >
                </ReactTable>
                <Modal isOpen={this.state.update} toggle={this.toggleUpdateForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleUpdateForm}> Update Caregiver: </ModalHeader>
                    <ModalBody>
                        <CaregiverUpdateForm caregiver={this.state.selectedCaregiver}  reloadHandlerUpdate={this.props.reloadHandler}/>
                    </ModalBody>
                </Modal>
            </div>


        );
    }
}

export default CaregiverTable;
