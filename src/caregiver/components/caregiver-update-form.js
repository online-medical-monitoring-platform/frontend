import React from 'react';
import validate from "../../patient/components/validators/patient-validators";
import Button from "react-bootstrap/Button";
import * as API_CAREGIVERS from "../api/caregiver-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';
import DatePicker from "react-date-picker";
import {addDays} from "date-fns";



class CaregiverUpdateForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);
        this.reloadHandler = this.props.reloadHandlerUpdate;
        this.state = {
            startDate: addDays(new Date(), -1),
            errorStatus: 0,
            error: null,

            formIsValid: false,

            birthdate: '',
            formControls: {
                name: {
                    value: this.props.caregiver.name,
                    placeholder: 'Caregiver\'s name...',
                    valid: true,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                birthdate: {
                    value: this.props.caregiver.birthdate,
                    valid: true,
                    touched: false,
                    validationRules: {
                        birthdateValidator:true
                    }
                },
                gender: {
                    value: this.props.caregiver.gender,
                    placeholder: 'M/F',
                    valid: true,
                    touched: false,
                    validationRules: {
                        genderValidator:true
                    }
                },
                address: {
                    value: this.props.caregiver.address,
                    placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                    touched: false,
                    valid:true
                }
            }
        };
        this.state.birthdate=this.props.caregiver.birthdate;
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeDate = this.handleChangeDate.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    toggleFormUpdate() {
        this.setState({collapseFormUpdate: !this.state.collapseFormUpdate});
    }

    handleChangeDate=date=>
    {
        this.setState({
            birthdate: (date.getUTCDate()+1)+"-"+(date.getMonth()+1)+"-"+date.getFullYear(),
            startDate: date
        });
        let formIsValid=false;
        if(!this.props.caregiver.birthdate)
            formIsValid=true;
        else
        if(this.props.caregiver.birthdate != (date.getUTCDate()+1)+"-"+(date.getMonth()+1)+"-"+date.getFullYear())
            formIsValid=true;
        let valid=formIsValid || this.state.formIsValid;
        console.log("Data:"+valid);
        this.setState({
            formIsValid: valid});
    };


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        if(name=="caregiver")
            updatedFormElement.valid=true;
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    updateCaregiver(caregiver) {
        return API_CAREGIVERS.updateCaregiver(caregiver,this.props.caregiver.id,(result, status, error) => {
            if (result !== null && (status === 202)) {
                console.log("Successfully updated caregiver with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }

    handleSubmit() {
            let caregiver = {
                name: this.state.formControls.name.value,
                birthdate: this.state.birthdate,
                gender: this.state.formControls.gender.value,
                address: this.state.formControls.address.value,
                username: this.props.caregiver.username,
                password: this.props.caregiver.password
            }
            this.updateCaregiver(caregiver);

    }


    render() {
        return (
            <div>

                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='birthdate'>
                    <Label for='birthdateField'> Birthdate:  </Label>
                    <Input defaultValue={this.state.formControls.birthdate.value} disabled={true}/>
                    <Label for='birthdateField'> New value:  </Label>
                    <DatePicker name='birthdate' id='birthdateField'
                                onChange={this.handleChangeDate}
                                value={this.state.startDate}
                                defaultValue={this.state.formControls.birthdate.value}
                                touched={this.state.formControls.birthdate.touched? 1 : 0}
                                maxDate={addDays(new Date(), -1)}
                                clearIcon={null}
                                readOnly={true}
                                required

                    />
                    {this.state.formControls.birthdate.touched && !this.state.formControls.birthdate.valid &&
                    <div className={"error-message"}> * Birthdate must be before today!</div>}
                </FormGroup>

                <FormGroup id='gender'>
                    <Label for='genderField'> Gender: </Label>
                    <Input name='gender' id='genderField' placeholder={this.state.formControls.gender.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.gender.value}
                           touched={this.state.formControls.gender.touched? 1 : 0}
                           valid={this.state.formControls.gender.valid}
                           required
                    />
                    {this.state.formControls.gender.touched && !this.state.formControls.gender.valid &&
                    <div className={"error-message"}> * Gender can be M or F!</div>}
                </FormGroup>


                <FormGroup id='address'>
                    <Label for='addressField'> Address: </Label>
                    <Input name='address' id='addressField' placeholder={this.state.formControls.address.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.address.value}
                           touched={this.state.formControls.address.touched? 1 : 0}
                           valid={this.state.formControls.address.valid}
                           required
                    />
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}
export default CaregiverUpdateForm;

