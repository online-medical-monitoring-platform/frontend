import React from 'react';
import {
    Label
} from 'reactstrap';


import ReactTable from "react-table";

const columns = [
    {
        Header: "Name",
        accessor: "name",
        style:{
            textAlign: "center"
        },
        width: 250
    },
    {
        Header: "Birthdate",
        accessor: "birthdate",
        style:{
            textAlign: "center"
        },
        width: 150
    },
    {
        Header: "Gender",
        accessor: "gender",
        style:{
            textAlign: "center"
        },
        width: 100
    },
    {
        Header: "Address",
        accessor: "address",style:{
            textAlign: "center"
        },
        width: 300
    },{
        Header: "Medical record",
        accessor: "medicalRecord",
        style:{
            textAlign: "center"
        },
        width: 350
    }
];

const filters = [
    {
        accessor: 'name',
    }
];

class CaregiverDetails extends React.Component {




    constructor(props) {
        super(props);

        this.state = {
            patients: this.props.patients,
            caregiver:this.props.caregiver,
            plans:this.props.medicationPlans,
            prescriptions:[],
            show: false,

            id: 0,
            selected: false,
            collapseForm: false,
            collapseFormUpdate:false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
        }
        this.reload = this.reload.bind(this);

    }

    componentDidMount() {
        //this.findPlans();
    }

    reload() {
        if(this.state.isLoadedPlans) {
            this.setState({
                show: false
            });
        }
    }


    render() {

        //  console.log(this.props.plans.indexOf(0));
      //  console.log("din details");
       // console.log(this.state.patients);

        //console.log("in render " +this.state.specificPlans);


        return (
           <div>
               <div style={{backgroundColor:'#ede7d8'}}>
                <Label sm={{ offset: 1}}><b>Name: </b>{this.state.caregiver.name}</Label>
                <br/>
                <Label sm={{ offset: 1}}><b>Birthdate: </b>{this.state.caregiver.birthdate}</Label>
                <br/>
                <Label sm={{ offset: 1}}><b>Gender: </b>{this.state.caregiver.gender}</Label>
                <br/>
                <Label sm={{ offset: 1}}><b>Address: </b>{this.state.caregiver.address}</Label>
                <br/>
                <Label sm={{ offset: 1}}><b>Username: </b>{this.state.caregiver.username} <b>Password: </b>{this.state.caregiver.password}</Label>
                </div>
               <br/>
                <div>
                    <h4 style={{ marginLeft: '180px'}}>Patients</h4>
                <ReactTable
                    className="-striped -highlight"
                    data={this.state.patients}
                    search={filters}
                    columns={columns}
                    defaultPageSize={10}
                    style={{ marginLeft: '180px', width: '1200px'}}
                >
                </ReactTable>
                </div>
               <br/>
            </div>
        )

    }
}/*

{  this.state.plans.map(
                    plan=> (
                        <div key={plan.id} style={{backgroundColor:'#fff3d4', padding:'30px'}}>
                            <Label sm={{offset: 1} } ><b>Medical plan name : </b>{plan.name}</Label>
                            <br/>
                            <Label sm={{offset: 1} } >Period: {" " +plan.periodStart +" <---> "+plan.periodStop}</Label>
                            <br/>
                            <PatientMedicationPlans medicationPlan={plan}/>
                        </div>
                    ))}


{ this.state.show && this.state.specificPlans.map( plan=> (
                        <div>
                            <Label sm={{offset: 1}}><b>Medical plan name : </b>{JSON.parse(plan).name}</Label>
                            <br/>
                        </div>
                    ))}
                <Label sm={{ offset: 1}}><b>Username: </b>{this.state.show && JSON.parse(this.state.specificPlans[0]).name} <b>Password: </b>{this.state.patient.password}</Label>


{this.state.plans.map( plan=>(
    <div>
        <Label sm={{ offset: 1}}><b>Name: </b>{plan.name}</Label>
        <br/>
        <Label sm={{ offset: 1}}><b>Start date: </b>{plan.periodStart}</Label>
        <Label sm={{ offset: 1}}><b>Stop date: </b>{plan.periodStop}</Label>
        <br/>
    </div>

))}*/

export default CaregiverDetails;
