import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    patient: '/patient',
    caregiver: '/caregiver',
    medicationPlan: '/medicationPlan'
};

function getPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.patient, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.patient +"/"+ params, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientByUser(username, password ,callback){
    let request = new Request(HOST.backend_api + endpoint.patient +"/"+ username+"/"+password, {
        method: 'GET'
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function getCaregiverByPatient(params, callback){
    let request = new Request(HOST.backend_api + endpoint.patient + params.id+ endpoint.caregiver, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicationPlans(params, callback){
    let request = new Request(HOST.backend_api + endpoint.patient + "/"+params+ endpoint.medicationPlan, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postPatient(patient, callback){
    let request = new Request(HOST.backend_api + endpoint.patient , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deletePatient(params,callback){
    let request = new Request(HOST.backend_api + endpoint.patient +'/'+ params, {
        method: 'DELETE',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    });

    console.log("URL: "+request.url);
    RestApiClient.performRequest(request, callback);
}

function updatePatient(patient,params, callback){
    let request = new Request(HOST.backend_api + endpoint.patient +'/'+ params, {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}



export {
    getPatients,
    getPatientById,
    postPatient,
    deletePatient,
    getCaregiverByPatient,
    updatePatient,
    getPatientByUser,
    getMedicationPlans
};
