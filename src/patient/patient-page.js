import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Card,
    CardHeader,
} from 'reactstrap';
import * as API_PATIENTS from "../patient/api/patient-api";
import PatientDetails from "./patient-details";
import {Redirect} from "react-router-dom";



class PatientPage extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.logoutHandle=this.logoutHandle.bind(this);

        this.state = {
            id: '',
            selected: false,
            plans: [],
            isLoaded: false,
            isLoadedPlans: false,
            patient:null,
            redirect:false,
            errorStatus: 0,
            error: null
        }

    }

    logoutHandle(){
        localStorage.clear();
        this.setState({
            redirect:true
        })
    }

    componentDidMount() {
        this.fetchMedicationPlans();
        this.fetchPatient();
    }

    fetchPatient() {
        return API_PATIENTS.getPatientById(Number(localStorage.getItem('id')),(result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    patient: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchMedicationPlans() {
        return API_PATIENTS.getMedicationPlans(Number(localStorage.getItem('id')),(result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    plans: result,
                    isLoadedPlans: true
                })
                console.log("Din fetch"+this.state.plans[0]);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    reload() {
     if(this.state.isLoadedPlans) {
         this.setState({
             isLoaded: false,
             isLoadedPlans: false
         });
         this.toggleForm();
         this.fetchMedicationPlans();
         this.fetchPatient();
     }
    }



    render() {
     /*
        console.log(this.state.patient);
        console.log("in render in page plan[0]");
        console.log(this.state.plans);
*/
        if(this.state.redirect)
        {
            return <Redirect to={'/login'}/>;
        }
        return (
            <div>
                <CardHeader>
                    <strong style={{fontSize:'30px'}}> Patient  </strong>
                    <button style={{marginLeft:'20px'}} onClick={this.logoutHandle} >Log out</button>
                </CardHeader>
                <Card style={{backgroundColor:'#ede7d8'}}>
                    {this.state.isLoadedPlans && this.state.isLoaded &&  <PatientDetails  patient={this.state.patient} medicationPlans={this.state.plans}/>}
                    {this.state.errorStatus > 0 && <APIResponseErrorMessage
                        errorStatus={this.state.errorStatus}
                        error={this.state.error}
                    />   }

                </Card>
            </div>
        )

    }
}


export default PatientPage;
