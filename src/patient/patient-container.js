import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import PatientForm from "../patient/components/patient-form";

import * as API_PATIENTS from "../patient/api/patient-api";
import * as API_CAREGIVERS from"../caregiver/api/caregiver-api"
import PatientTable from "./components/patient-table";
import NavigationBar from "../navigation-bar";



class PatientContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.reloadUpdate = this.reloadUpdate.bind(this);

        this.state = {
            delete: false,
            id: '',
            selected: false,
            collapseForm: false,
            collapseFormUpdate:false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            caregivers:[]
        }

    }

    componentDidMount() {
        this.fetchPatients();
        this.fetchCaregivers();
    }

    fetchCaregivers(){
        return API_CAREGIVERS.getCaregivers((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    caregivers: result,
                //    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }



    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchPatients();
    }
    reloadUpdate() {
        this.setState({
            isLoaded: false
        });
        this.fetchPatients();
    }

    fetchPatients() {
        return API_PATIENTS.getPatients((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    render() {
      /*  if(localStorage.getItem('role')==='')
        {
            return (<PatientPage patientId={3}/>)
        }*/
        return (
            <div>
                <NavigationBar />
                <CardHeader>
                    <strong> Patient Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Patient </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '10.5', offset: '1'}}>
                            {this.state.isLoaded && <PatientTable patients = {this.state.tableData} caregivers={this.state.caregivers} reloadHandler={this.reloadUpdate}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>
                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Patient: </ModalHeader>
                    <ModalBody>
                        <PatientForm reloadHandler={this.reload} caregivers={this.state.caregivers}/>
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}


export default PatientContainer;
