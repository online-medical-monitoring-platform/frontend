import React from 'react';
import {Label
} from 'reactstrap';

import PatientMedicationPlans from "./patient-medicationPlans";
import * as API_PLANS from "../medicationPlan/medicationPlan-api";



class PatientDetails extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            patient: this.props.patient,
            plans:this.props.medicationPlans,
            prescriptions:[],
            show: false,

            id: 0,
            selected: false,
            collapseForm: false,
            collapseFormUpdate:false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            caregivers:[]
        }
        this.reload = this.reload.bind(this);

    }

    componentDidMount() {
            //this.findPlans();
    }

    reload() {
        if(this.state.isLoadedPlans) {
            this.setState({
                show: false
            });
        }
    }

    fetchMedicalPrescriptions(id){

        return API_PLANS.getMedicationPrescriptions(id,(result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    prescriptions: result,
                    show: true
                })
                console.log("Din fetch"+this.state.prescriptions[0]);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }


    findPlans() {
      /*  for (let i = 0; i < this.state.plans.length; i++) {
            if (this.state.plans[i].patient.id === this.state.patient.id)
                this.state.specificPlans.push(this.state.plans[i]);
        }*/
        let i= 0;
        this.state.plans.forEach(plan =>{
            if (plan.patient.id === this.state.patient.id){
                let aux ={
                    name: this.state.plans[i].name,
                    periodStart: this.state.plans[i].periodStart,
                    periodStop: this.state.plans[i].periodStop,
                    patient: this.state.plans[i].patient
                }
                this.state.specificPlans.push(JSON.stringify(aux));
                i++;
               // console.log("aux"+this.state.specificPlans[0]);
            this.setState({
                show:true
            });
            }}
        );
     /*   console.log("find plans "+JSON.parse(this.state.specificPlans[2]));
        this.state.specificPlans.forEach(plan =>{
            console.log("NUme tratament"+ JSON.parse(plan).name);
        });*/
    }

    render() {

      //  console.log(this.props.plans.indexOf(0));
        console.log("din details");
        console.log(this.state.plans);

        //console.log("in render " +this.state.specificPlans);
        const columns = [
            {
                Header: "Name",
                accessor: "name",
                style:{
                    textAlign: "center"
                },
                width: 250
            },
            {
                Header: "Birthdate",
                accessor: "birthdate",
                style:{
                    textAlign: "center"
                },
                width: 150
            },
            {
                Header: "Gender",
                accessor: "gender",
                style:{
                    textAlign: "center"
                },
                width: 100
            },
            {
                Header: "Address",
                accessor: "address",style:{
                    textAlign: "center"
                },
                width: 300
            }]


        return (
            <div >
                <Label sm={{ offset: 1}}><b>Name: </b>{this.state.patient.name}</Label>
                <br/>
                <Label sm={{ offset: 1}}><b>Birthdate: </b>{this.state.patient.birthdate}</Label>
                <br/>
                <Label sm={{ offset: 1}}><b>Gender: </b>{this.state.patient.gender}</Label>
                <br/>
                <Label sm={{ offset: 1}}><b>Address: </b>{this.state.patient.address}</Label>
                <br/>
                <Label sm={{ offset: 1}}><b>Medical record : </b>{this.state.patient.medicalRecord}</Label>
                <br/>
                <Label sm={{ offset: 1}}><b>Username: </b>{this.state.patient.username} <b>Password: </b>{this.state.patient.password}</Label>
               <br/>

                {  this.state.plans.map(
                    plan=> (
                        <div key={plan.id} style={{backgroundColor:'#fff3d4', padding:'30px'}}>
                            <Label sm={{offset: 1} } ><b>Medical plan name : </b>{plan.name}</Label>
                            <br/>
                            <Label sm={{offset: 1} } >Period: {" " +plan.periodStart +" <---> "+plan.periodStop}</Label>
                            <br/>
                            <PatientMedicationPlans medicationPlan={plan}/>
                        </div>
                    ))}
            </div>
        )

    }
}/*
{ this.state.show && this.state.specificPlans.map( plan=> (
                        <div>
                            <Label sm={{offset: 1}}><b>Medical plan name : </b>{JSON.parse(plan).name}</Label>
                            <br/>
                        </div>
                    ))}
                <Label sm={{ offset: 1}}><b>Username: </b>{this.state.show && JSON.parse(this.state.specificPlans[0]).name} <b>Password: </b>{this.state.patient.password}</Label>


{this.state.plans.map( plan=>(
    <div>
        <Label sm={{ offset: 1}}><b>Name: </b>{plan.name}</Label>
        <br/>
        <Label sm={{ offset: 1}}><b>Start date: </b>{plan.periodStart}</Label>
        <Label sm={{ offset: 1}}><b>Stop date: </b>{plan.periodStop}</Label>
        <br/>
    </div>

))}*/

export default PatientDetails;
