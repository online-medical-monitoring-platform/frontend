import React from 'react';
import {Label
} from 'reactstrap';

import * as API_PLANS from "../medicationPlan/medicationPlan-api";



class PatientMedicationPlans extends React.Component {

    constructor(props) {
        super(props);

        this.state = {

            prescriptions:[],
            plan:this.props.medicationPlan,

            isLoaded: false,

            id: 0,
            selected: false,
            collapseForm: false,
            collapseFormUpdate:false,
            tableData: [],
            errorStatus: 0,
            error: null,
            caregivers:[]
        }
        this.reload = this.reload.bind(this);

    }
    componentDidMount() {
        this.fetchMedicalPrescriptions();
    }

    reload() {
        if(this.state.isLoadedPlans) {
            this.setState({
                show: false
            });
              this.fetchMedicalPrescriptions();
        }
    }

    fetchMedicalPrescriptions(){

        return API_PLANS.getMedicationPrescriptions(this.state.plan.id,(result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    prescriptions: result,
                    show: true
                })
                console.log("Din fetch"+this.state.prescriptions[0]);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    render() {

        //  console.log(this.props.plans.indexOf(0));
        console.log("din Planu medica  prescriptiile sunt"+ this.state.plan.id+ "  ");
        console.log(this.state.prescriptions);

        ;

        return (
            <div style={{padding:'10 px', backgroundColor:'#ede7d8'}}>
                { this.state.show && this.state.prescriptions.map(
                    prescription=> (
                        <div style={{backgroundColor:'#bfe1f1', paddingLeft:'20px'} }key={prescription.id}>
                            <Label sm={{offset: 1}}><b>{prescription.medication.name}</b>
                                {"  "+prescription.medication.dosage}<i>  Intake:</i>{"   " + prescription.intake +" "}
                                <i>Side effects:</i> {"   "+prescription.medication.sideEffects}
                            </Label>
                        </div>
                    ))}
            </div>
        )

    }
}
export default PatientMedicationPlans;
