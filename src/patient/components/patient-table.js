import React, { Component } from 'react';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import PatientUpdateForm from "./patient-update-form";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import * as API_PATIENTS from "../api/patient-api";


const filters = [
    {
        accessor: 'name',
    }
];

class PatientTable extends Component {

    constructor(props) {
        super(props);
        this.state = {
            update: false,
            patients: this.props.patients,
            selectedPatient: '',
            caregivers : this.props.caregivers,

        }
        this.toggleUpdateForm = this.toggleUpdateForm.bind(this);
        this.updatePatient = this.updatePatient.bind(this);
    }

    toggleUpdateForm() {

        this.setState({
            update: !this.state.update
        });


    }

    deletePatient(id) {
        return API_PATIENTS.deletePatient(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted patient with id: " + result);
                this.props.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }

    updatePatient(id) {
        console.log("caregivers:"+this.props.caregivers.size);
        const index = this.state.patients.findIndex(patients => {
            return patients.id === id
        });
        this.setState({
            selectedPatient: this.state.patients[index],
            update: !this.state.update
        });
        console.log(this.state.patients[index]);
        this.toggleUpdateForm();
    }


    render() {
        const data = this.state.patients;
        const columns = [
            {
                Header: "Name",
                accessor: "name",
                style:{
                    textAlign: "center"
                },
                width: 180
            },
            {
                Header: "Birthdate",
                accessor: "birthdate",
                style:{
                    textAlign: "center"
                },
                width: 100
            },
            {
                Header: "Gender",
                accessor: "gender",
                style:{
                    textAlign: "center"
                },
                width: 65
            },
            {
                Header: "Address",
                accessor: "address",style:{
                    textAlign: "center"
                },
                width: 300
            },
            {
                Header: "Medical record",
                accessor: "medicalRecord",
                style:{
                    textAlign: "center"
                },
                width: 250
            }, {
                Header: "Username",
                accessor: "username",
                style:{
                    textAlign: "center"
                },
                width: 150
            },{
                Header: "Password",
                accessor: "password",
                style:{
                    textAlign: "center"
                },
                width: 150
            },
            {
                Header: "Actions",
                filterable: false,
                sortable: false,
                resizable: false,
                Cell: patients =>{
                    return(
                        <div>
                        <button style={{background: "red", color: "#fefefe",float:"left"}}
                                onClick={(e)=> {
                                    this.deletePatient(patients.original.id);
                                }}
                        >Delete</button>
                    <button style={{background: "blue", color: "#fefefe" ,float:"right"}}
                            onClick={(e)=> {
                                this.updatePatient(patients.original.id);
                            }}
                    >Update</button>
                        </div>
                    )},
                width: 150,
                maxWidth: 150,
                minWidth: 150,
                style:{
                textAlign: "center"
                },
            }
        ]
        return (
            <div>
                <ReactTable
                    className="-striped -highlight"
                    data={data}
                    search={filters}
                    columns={columns}
                    defaultPageSize={10}
                >
                </ReactTable>
                <Modal isOpen={this.state.update} toggle={this.toggleUpdateForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleUpdateForm}> Update Patient: </ModalHeader>
                    <ModalBody>
                        <PatientUpdateForm patient={this.state.selectedPatient} caregivers={this.props.caregivers} reloadHandlerUpdate={this.props.reloadHandler}/>
                    </ModalBody>
                </Modal>
            </div>


        );
    }
}

export default PatientTable;
