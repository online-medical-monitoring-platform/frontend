import React from 'react';
import validate from "./validators/patient-validators";
import Button from "react-bootstrap/Button";
import * as API_PATIENTS from "../api/patient-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';
import DatePicker from "react-date-picker";
import {addDays} from "date-fns";



class PatientUpdateForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);
        this.reloadHandler = this.props.reloadHandlerUpdate;
        this.state = {

            caregivers : this.props.caregivers,

            startDate: addDays(new Date(), -1),
            errorStatus: 0,
            error: null,

            formIsValid: false,

            caregiverName: '',
            caregiver: '',
            birthdate: '',
            formControls: {
                name: {
                    value: this.props.patient.name,
                    placeholder: 'Patient\'s name...',
                    valid: true,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                birthdate: {
                    value: this.props.patient.birthdate,
                    valid: true,
                    touched: false,
                    validationRules: {
                        birthdateValidator:true
                    }
                },
                gender: {
                    value: this.props.patient.gender,
                    placeholder: 'M/F',
                    valid: true,
                    touched: false,
                    validationRules: {
                        genderValidator:true
                    }
                },
                address: {
                    value: this.props.patient.address,
                    placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                    touched: false,
                    valid:true
                },
                medicalRecord: {
                    value: this.props.patient.medicalRecord,
                    placeholder: 'How do you feel?...',
                    valid: true,
                    touched: false,
                    validationRules: {
                        minLength: 3
                    }
                }
            }
        };
        if(this.props.patient.caregiver) {
            this.state.caregiverName = this.props.patient.caregiver.name;
            this.state.caregiver=this.props.patient.caregiver;
        }
        this.state.birthdate=this.props.patient.birthdate;
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeDate = this.handleChangeDate.bind(this);
        this.handleChangeCaregiver = this.handleChangeCaregiver.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    toggleFormUpdate() {
        this.setState({collapseFormUpdate: !this.state.collapseFormUpdate});
    }

    handleChangeDate=date=>
    {
        this.setState({
            birthdate: (date.getUTCDate()+1)+"-"+(date.getMonth()+1)+"-"+date.getFullYear(),
            startDate: date
        });
        let formIsValid=false;
        if(!this.props.patient.birthdate)
            formIsValid=true;
        else
            if(this.props.patient.birthdate != (date.getUTCDate()+1)+"-"+(date.getMonth()+1)+"-"+date.getFullYear())
                formIsValid=true;
        let valid=formIsValid || this.state.formIsValid;
        console.log("Data:"+valid);
        this.setState({
            formIsValid: valid});
    };

    handleChangeCaregiver=caregiver=>
    {
        var id = caregiver.nativeEvent.target.selectedIndex;
        console.log('native: ' + caregiver.nativeEvent.target[id].text+ " id:"+ id);
        if(! (caregiver.nativeEvent.target[id].text === "--"))
            this.setState({
                    caregiver: this.state.caregivers[id-1]
                }
            )

        let formIsValid;
        if(caregiver.nativeEvent.target[id].text==="--" || caregiver.nativeEvent.target[id].text===this.state.caregiverName)
            formIsValid=false;
        else
            formIsValid=true;
        let valid=formIsValid || this.state.formIsValid;

        if(valid)
        {console.log("Caregiver"+this.state.caregiver);
        console.log("Caregiver in sir"+this.state.caregivers[id-1]);}
        this.setState({
            formIsValid: valid});
    };


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        if(name=="caregiver")
            updatedFormElement.valid=true;
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    updatePatient(patient) {
        return API_PATIENTS.updatePatient(patient,this.props.patient.id,(result, status, error) => {
            if (result !== null && (status === 202)) {
                console.log("Successfully updated patient with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }

    handleSubmit() {
        if((!this.props.patient.caregiver && !this.state.caregiver)) {
            let patient = {
                name: this.state.formControls.name.value,
                birthdate: this.state.birthdate,
                gender: this.state.formControls.gender.value,
                address: this.state.formControls.address.value,
                medicalRecord: this.state.formControls.medicalRecord.value,
                username: this.props.patient.username,
                password: this.props.patient.password
            }
            console.log("Fara caregiver"+patient);
            this.updatePatient(patient);
        }
        else{

            let patient = {
                name: this.state.formControls.name.value,
                birthdate: this.state.birthdate,
                gender: this.state.formControls.gender.value,
                address: this.state.formControls.address.value,
                medicalRecord: this.state.formControls.medicalRecord.value,
                caregiver: this.state.caregiver,
                username: this.props.patient.username,
                password: this.props.patient.password
            }
            console.log("Cu caregiver"+patient);
            this.updatePatient(patient);

        }
    }


    render() {
        return (
            <div>

                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='birthdate'>
                    <Label for='birthdateField'> Birthdate:  </Label>
                    <Input defaultValue={this.state.formControls.birthdate.value} disabled={true}/>
                    <Label for='birthdateField'> New value:  </Label>
                    <DatePicker name='birthdate' id='birthdateField'
                                onChange={this.handleChangeDate}
                                value={this.state.startDate}
                                defaultValue={this.state.formControls.birthdate.value}
                                touched={this.state.formControls.birthdate.touched? 1 : 0}
                                maxDate={addDays(new Date(), -1)}
                                clearIcon={null}
                                readOnly={true}
                                required

                    />
                    {this.state.formControls.birthdate.touched && !this.state.formControls.birthdate.valid &&
                    <div className={"error-message"}> * Birthdate must be before today!</div>}
                </FormGroup>

                <FormGroup id='gender'>
                    <Label for='genderField'> Gender: </Label>
                    <Input name='gender' id='genderField' placeholder={this.state.formControls.gender.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.gender.value}
                           touched={this.state.formControls.gender.touched? 1 : 0}
                           valid={this.state.formControls.gender.valid}
                           required
                    />
                    {this.state.formControls.gender.touched && !this.state.formControls.gender.valid &&
                    <div className={"error-message"}> * Gender can be M or F!</div>}
                </FormGroup>


                <FormGroup id='address'>
                    <Label for='addressField'> Address: </Label>
                    <Input name='address' id='addressField' placeholder={this.state.formControls.address.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.address.value}
                           touched={this.state.formControls.address.touched? 1 : 0}
                           valid={this.state.formControls.address.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='medicalRecord'>
                    <Label for='medicalRecordField'> Medical record: </Label>
                    <Input name='medicalRecord' id='medicalRecordField' placeholder={this.state.formControls.medicalRecord.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.medicalRecord.value}
                           touched={this.state.formControls.medicalRecord.touched? 1 : 0}
                           valid={this.state.formControls.medicalRecord.valid}
                           required
                    />
                    {this.state.formControls.medicalRecord.touched && !this.state.formControls.medicalRecord.valid &&
                    <div className={"error-message"}> * Medical record must describe at least the general state of the pacient!</div>}
                </FormGroup>

                <FormGroup id='caregiver'>
                    <Label for='caregiverField'> Caregiver curent: </Label>
                    <Input defaultValue={this.state.caregiverName} disabled={true}/>
                    <Label for='caregiverField'> New value:  </Label>
                    <select name='caregiver' id='caregiverField'
                            onChange={this.handleChangeCaregiver}>
                        <option>--</option>
                        {this.state.caregivers.map( caregiver=>(
                            <option key={caregiver.id} value={caregiver.name}>
                                {caregiver.name}
                            </option>
                        ))}
                    </select>
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}
export default PatientUpdateForm;

