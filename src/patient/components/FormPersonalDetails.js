import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import TextField from '@material-ui/core/TextField';
//import Button from '@material-ui/core/Button';
import validate from "./validators/patient-validators";
import Button from "react-bootstrap/Button";
import * as API_PATIENTS from "../api/patient-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';
import DatePicker from 'react-date-picker';
import { addDays } from 'date-fns';


export class FormPersonalDetails extends Component {



    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        alert(this.props.userId);

        this.state = {

            caregivers : [
                {id:"1", name:"caregiver1"},
                {id:"2", name:"caregiver2"}],

            userID:0,
            patientID:1,

            errorStatus: 0,
            error: null,

            formIsValid: false,

            startDate: addDays(new Date(), -1),
            formControls: {
                name: {
                    value: '',
                    placeholder: 'Patient\'s name...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                birthdate: {
                    value: '',
                    valid: true,
                    touched: false,
                    validationRules: {
                        birthdateValidator:true
                    }
                },
                gender: {
                    value: '',
                    placeholder: 'M/F',
                    valid: false,
                    touched: false,
                    validationRules: {
                        genderValidator:true
                    }
                },
                address: {
                    value: '',
                    placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                    touched: false,
                },
                medicalRecord: {
                    value: '',
                    placeholder: 'How do you feel?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3
                    }
                },
                caregiver:{
                    value: '',
                    placeholder: 'Caregiver',
                    touched: false,
                    valid:true
                }

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    handleChangeDate=date=>
    {
        this.setState({
            startDate: date
        });
        console.log(date);
    };

    handleChangeCaregiver=caregiver=>
    {
        var id = caregiver.nativeEvent.target.selectedIndex;
        console.log('native: ' + caregiver.nativeEvent.target[id].text+ " id:"+ id);

    };

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        if(name=="caregiver")
            updatedFormElement.valid=true;
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    registerPatient(patient) {
        return API_PATIENTS.postPatient(patient, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted patient with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }

    registerUser(user){
       /* return API_USERS.postUser(user, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted user with id: " + result);
                this.state.userId=result;
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });*/
    }

    handleSubmit() {
        let caregiver;
        let user={
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value
        }

        console.log(user);
        this.registerUser(user).then( ()=> {

            if (this.state.formControls.caregiver.value === "") {
                console.log("E gol la caregiver");
                caregiver = null;
            } else
                caregiver = this.state.formControls.caregiver.value;

            let user1 = {
                id: this.state.userID
            }

            let patient = {
                name: this.state.formControls.name.value,
                birthdate: this.state.formControls.birthdate.value,
                gender: this.state.formControls.gender.value,
                address: this.state.formControls.address.value,
                medicalRecord: this.state.formControls.medicalRecord.value,
                caregiver: caregiver,
                userApp: user1
            };

            console.log(patient);
            this.registerPatient(patient);
        } );
    }


    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    };

    back = e => {
        e.preventDefault();
        this.props.prevStep();
    };




render() {
        const { values, handleChange } = this.props;
        return (
            <div>

                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='birthdate'>
                    <Label for='birthdateField'> Birthdate: </Label>
                    <DatePicker name='birthdate' id='birthdateField'
                           onChange={this.handleChangeDate}
                           value={this.state.startDate}
                           defaultValue={this.state.formControls.birthdate.value}
                           touched={this.state.formControls.birthdate.touched? 1 : 0}
                           valid={this.state.formControls.birthdate.valid}
                                maxDate={addDays(new Date(), -1)}
                                placeholderText="Select a date before today"
                           required

                    />
                    {this.state.formControls.birthdate.touched && !this.state.formControls.birthdate.valid &&
                    <div className={"error-message"}> * Birthdate must be before today!</div>}
                </FormGroup>

                <FormGroup id='gender'>
                    <Label for='genderField'> Gender: </Label>
                    <Input name='gender' id='genderField' placeholder={this.state.formControls.gender.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.gender.value}
                           touched={this.state.formControls.gender.touched? 1 : 0}
                           valid={this.state.formControls.gender.valid}
                           required
                    />
                    {this.state.formControls.gender.touched && !this.state.formControls.gender.valid &&
                    <div className={"error-message"}> * Gender can be M or F!</div>}
                </FormGroup>


                <FormGroup id='address'>
                    <Label for='addressField'> Address: </Label>
                    <Input name='address' id='addressField' placeholder={this.state.formControls.address.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.address.value}
                           touched={this.state.formControls.address.touched? 1 : 0}
                           valid={this.state.formControls.address.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='medicalRecord'>
                    <Label for='medicalRecordField'> Medical record: </Label>
                    <Input name='medicalRecord' id='medicalRecordField' placeholder={this.state.formControls.medicalRecord.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.medicalRecord.value}
                           touched={this.state.formControls.medicalRecord.touched? 1 : 0}
                           valid={this.state.formControls.medicalRecord.valid}
                           required
                    />
                    {this.state.formControls.medicalRecord.touched && !this.state.formControls.medicalRecord.valid &&
                    <div className={"error-message"}> * Medical record must describe at least the general state of the pacient!</div>}
                </FormGroup>

                <FormGroup id='caregiver'>
                    <Label for='caregiverField'> Caregiver: </Label>
                    <select name='caregiver' id='caregiverField'
                            onChange={this.handleChangeCaregiver}>
                        <option>--</option>
                        {this.state.caregivers.map( caregiver=>(
                            <option key={caregiver.id} value={caregiver.name}>
                                {caregiver.name}
                            </option>
                        ))}
                    </select>
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        );
    }
}

export default FormPersonalDetails;

/* <DropDownList name='caregiver' id='caregiverField'
                           onChange={this.handleChangeCaregiver}
                          // defaultValue={this.state.formControls.caregiver.value}
                           data={this.caregivers}
                                  filterable={true}
                         //  touched={1}
                    />*/
