import React from 'react';
import validate from "./validators/patient-validators";
import Button from "react-bootstrap/Button";
import * as API_PATIENTS from "../api/patient-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';
import DatePicker from "react-date-picker";
import {addDays} from "date-fns";



class PatientForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;
        this.state = {

            caregivers : this.props.caregivers,
            startDate: addDays(new Date(), -1),


            errorStatus: 0,
            error: null,

            formIsValid: false,

            caregiver: '',
            birthdate:'',
            formControls: {
                name: {
                    value: '',
                    placeholder: 'Patient\'s name...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                birthdate: {
                    value: '',
                    valid: true,
                    touched: false,
                    validationRules: {
                        birthdateValidator:true
                    }
                },
                gender: {
                    value: '',
                    placeholder: 'M/F',
                    valid: false,
                    touched: false,
                    validationRules: {
                        genderValidator:true
                    }
                },
                address: {
                    value: '',
                    placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                    touched: false,
                },
                medicalRecord: {
                    value: '',
                    placeholder: 'How do you feel?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3
                    }
                },
                caregiver:{
                    value: '',
                    placeholder: 'Caregiver',
                    touched: false,
                    valid:true
                },
                username: {
                    value: '',
                    placeholder: 'Username',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 5,
                        isRequired: true
                    }
                },
                password: {
                    value: '',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 5,
                        isRequired: true
                    }
                }

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeDate = this.handleChangeDate.bind(this);
        this.handleChangeCaregiver = this.handleChangeCaregiver.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    handleChangeDate=date=>
    {
        let today= new Date();
        if(date< today)
        this.setState({
            birthdate: (date.getUTCDate()+1)+"-"+(date.getMonth()+1)+"-"+date.getFullYear(),
            startDate: date
        });
    };

    handleChangeCaregiver=caregiver=>
    {
        var id = caregiver.nativeEvent.target.selectedIndex;
        console.log('native: ' + caregiver.nativeEvent.target[id].text+ " id:"+ id);
        if(! (caregiver.nativeEvent.target[id].text === "--"))
            this.setState({
                    caregiver: this.state.caregivers[id-1]
                }
            )
    };


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        if(name=="caregiver")
            updatedFormElement.valid=true;
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    registerPatient(patient) {
        return API_PATIENTS.postPatient(patient, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted patient with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }

    handleSubmit() {
        if(this.state.caregiver==='')
        {let patient = {
                name: this.state.formControls.name.value,
                birthdate: this.state.birthdate,
                gender: this.state.formControls.gender.value,
                address: this.state.formControls.address.value,
                medicalRecord: this.state.formControls.medicalRecord.value,
                username:this.state.formControls.username.value,
                password:this.state.formControls.password.value
            };
            this.registerPatient(patient);
        }
        else{
            let patient = {
                name: this.state.formControls.name.value,
                birthdate: this.state.birthdate,
                gender: this.state.formControls.gender.value,
                address: this.state.formControls.address.value,
                medicalRecord: this.state.formControls.medicalRecord.value,
                caregiver: this.state.caregiver,
                username:this.state.formControls.username.value,
                password:this.state.formControls.password.value
            };
            this.registerPatient(patient);
        }
    }


    render() {
        return (
            <div>

                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='birthdate'>
                    <Label for='birthdateField'> Birthdate: </Label>
                    <DatePicker name='birthdate' id='birthdateField'
                                onChange={this.handleChangeDate}
                                value={this.state.startDate}
                                defaultValue={this.state.formControls.birthdate.value}
                                touched={this.state.formControls.birthdate.touched? 1 : 0}
                                maxDate={addDays(new Date(), -1)}
                                clearIcon={null}
                                readOnly={true}
                                dateFormat="dd/MM/YYYY"
                                required

                    />
                    {this.state.formControls.birthdate.touched && !this.state.formControls.birthdate.valid &&
                    <div className={"error-message"}> * Birthdate must be before today!</div>}
                </FormGroup>

                <FormGroup id='gender'>
                    <Label for='genderField'> Gender: </Label>
                    <Input name='gender' id='genderField' placeholder={this.state.formControls.gender.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.gender.value}
                           touched={this.state.formControls.gender.touched? 1 : 0}
                           valid={this.state.formControls.gender.valid}
                           required
                    />
                    {this.state.formControls.gender.touched && !this.state.formControls.gender.valid &&
                    <div className={"error-message"}> * Gender can be M or F!</div>}
                </FormGroup>


                <FormGroup id='address'>
                    <Label for='addressField'> Address: </Label>
                    <Input name='address' id='addressField' placeholder={this.state.formControls.address.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.address.value}
                           touched={this.state.formControls.address.touched? 1 : 0}
                           valid={this.state.formControls.address.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='medicalRecord'>
                    <Label for='medicalRecordField'> Medical record: </Label>
                    <Input name='medicalRecord' id='medicalRecordField' placeholder={this.state.formControls.medicalRecord.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.medicalRecord.value}
                           touched={this.state.formControls.medicalRecord.touched? 1 : 0}
                           valid={this.state.formControls.medicalRecord.valid}
                           required
                    />
                    {this.state.formControls.medicalRecord.touched && !this.state.formControls.medicalRecord.valid &&
                    <div className={"error-message"}> * Medical record must describe at least the general state of the pacient!</div>}
                </FormGroup>

                <FormGroup id='caregiver'>
                    <Label for='caregiverField'> Caregiver: </Label>
                    <select name='caregiver' id='caregiverField'
                            onChange={this.handleChangeCaregiver}>
                        <option>--</option>
                        {this.state.caregivers.map( caregiver=>(
                            <option key={caregiver.id} value={caregiver.name}>
                                {caregiver.name}
                            </option>
                        ))}
                    </select>
                </FormGroup>

                <FormGroup id='username'>
                    <Label for='usernameField'> Userame: </Label>
                    <Input name='username' id='usernameField' placeholder={this.state.formControls.username.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.username.value}
                           touched={this.state.formControls.username.touched? 1 : 0}
                           valid={this.state.formControls.username.valid}
                           required
                    />
                    {this.state.formControls.username.touched && !this.state.formControls.username.valid &&
                    <div className={"error-message row"}> * Userame must have at least 5 characters </div>}
                </FormGroup>

                <FormGroup id='password'>
                    <Label for='passwordField'> Password: </Label>
                    <Input name='password' id='passwordField' placeholder={this.state.formControls.password.placeholder}
                           type='password'
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.password.value}
                           touched={this.state.formControls.password.touched? 1 : 0}
                           valid={this.state.formControls.password.valid}
                           required
                    />
                    {this.state.formControls.password.touched && !this.state.formControls.password.valid &&
                    <div className={"error-message"}> * Password must have at least 5 characters</div>}
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}
export default PatientForm;

/*
export default PatientForm;
import React, {Component, useEffect, useState} from 'react';
import UserForm from '../../user/components/user-form';
import FormPersonalDetails from './FormPersonalDetails';
import Confirm from './Confirm';

//const delay = () => new Promise(resolve => setTimeout(() => resolve("data fetched"), 4000));
//const [mounted, setMounted] = useState(true);


export class PatientForm extends Component {



    constructor(props) {
        super(props);

        this.setUserId = this.setUserId.bind(this);
        this.reloadHandler = this.props.reloadHandler;
        this.state = {
            step: 1,
            username: '',
            password: '',
            userId:'',
            firstName: '',
            lastName: '',
            email: ''
        }
        }

    // Proceed to next step
    nextStep = () => {
        //setMounted(!mounted);
        const { step } = this.state;
        this.setState({
            step: step + 1
        });
    };''

    // Go back to prev step
    prevStep = () => {
        const { step } = this.state;
        this.setState({
            step: step - 1
        });
    };

    useAsync(asyncFn, onSuccess) {
        useEffect(() => {
            let isMounted = true;
            asyncFn().then(data => {
                if (isMounted) onSuccess(data);
            });
            return () => {
                isMounted = false;
            };
        }, [asyncFn, onSuccess]);
    }

    // Handle fields change
    handleChange = input => e => {
        this.setState({ [input]: e.target.value });
    };

    setUserId (id){
        this.setState({
            userId:id
        });
    }

    render() {
        const { step } = this.state;
        const { username, password, email, occupation, city, bio } = this.state;
        const values = { username, password, email, occupation, city, bio };

        switch (step) {
            case 1:
                return (
                    <UserForm
                        nextStep={this.nextStep}
                        handleChange={this.handleChange}
                        values={values}
                        setUserId={this.setUserId}
                    />
                );
            case 2:
                return (
                    <FormPersonalDetails
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleChange={this.handleChange}
                        values={values}
                    />
                );
            case 3:
                return (
                    <Confirm
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        values={values}
                    />
                );
            default:
                (console.log('This is a multi-step form built with React.'))
        }
    }
}

export default PatientForm;
*/
