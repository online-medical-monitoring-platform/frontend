import React from 'react';
import validate from "../patient/components/validators/patient-validators";
import Button from "react-bootstrap/Button";
import * as API_MEDICATIONPLANS from "./medicationPlan-api";
import * as API_MEDICATIONPRESCRIPTIONS from "./medicationPrescription-api";

import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';
import DatePicker from "react-date-picker";
import {addDays} from "date-fns";
import NavigationBar from "../navigation-bar";



class MedicationPlanForm extends React.Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangePatient= this.handleChangePatient.bind(this);
        this.handleChangeDateStart= this.handleChangeDateStart.bind(this);
        this.handleChangeDateStop= this.handleChangeDateStop.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleSubmitMedicationPrescription=this.handleSubmitMedicationPrescription.bind(this);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;
        this.setCreated=this.props.setCreated;
        this.state = {

            patients: this.props.patients,
            medications:this.props.medications,

            planId:'',

            patient:'',
            selectedPatient:false,
            medication:'',
            selectedMedication:false,
            pressed: false,
            medicationAdded:false,

            start:(new Date()).getUTCDate()+"-"+((new Date()).getMonth()+1)+"-"+(new Date()).getFullYear(),
            stop:'',


            startDate: new Date(),
            stopDate: addDays(new Date(), +1),
            selecteddate1:false,
            selecteddate2:false,

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                name: {
                    value: '',
                    placeholder: 'Name for the medication plan...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                periodStart:{
                    value: '',
                    valid: true,
                    touched: false},
                periodStop: {
                    value: '',
                    valid: true,
                    touched: false},
                intake: {
                    value: '',
                    placeholder: 'Hours when the madication should be administrated',
                    valid: false,
                    touched: false
                },
            }
        }

        }



    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }



    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        if(name==='intake')
            localStorage.setItem('intake', event.target.value);

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }
        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    registerMedicationPlan(plan) {
        return API_MEDICATIONPLANS.postMedicationPlan(plan, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                this.setState({
                    planId:result,
                    pressed:true
                })
                localStorage.setItem('planId',result);
                //this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }

    registerMedicationIntoPlan(medicationPrescription) {
        return API_MEDICATIONPRESCRIPTIONS.postMedicationPrescription(medicationPrescription, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                this.setState({
                  //  planId:result,
                    medicationAdded:true
                })
             //   this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }



    handleChangeDateStart=date=>
    {
        let today= new Date();
        if(date>= today)
            this.setState({
                start: (date.getUTCDate()+1)+"-"+(date.getMonth()+1)+"-"+date.getFullYear(),
                startDate: date,
                selecteddate1:true
            });

    };
    handleChangeDateStop=date=>
    {
        let today= new Date();
        if(date> today)
            this.setState({
                stop: (date.getUTCDate()+1)+"-"+(date.getMonth()+1)+"-"+date.getFullYear(),
                stopDate: date,
                selecteddate2: true
            });
    };

    handleChangePatient=caregiver=>
    {
        var id = caregiver.nativeEvent.target.selectedIndex;
        console.log('native: ' + caregiver.nativeEvent.target[id].text+ " id:"+ id);
        if(! (caregiver.nativeEvent.target[id].text === "--")) {
            this.setState({
                patient: this.state.patients[id-1],
                selectedPatient:true
            });
        }
        else
            this.setState({
                selectedPatient: false
                }
            )
    };


    handleSubmit() {
        let medicationPlan = {
            name: this.state.formControls.name.value,
            periodStart: this.state.start,
            periodStop: this.state.stop,
            patient: this.state.patient
        };
        console.log("medication plan created:"+medicationPlan);
        console.log("Register"+this.state.patient.name+" plan "+this.state.formControls.name.value);
        localStorage.setItem('patientName',this.state.patient.name);
        localStorage.setItem('planName',this.state.formControls.name.value);
        this.registerMedicationPlan(medicationPlan);

    }

    handleChangeMedication=medication=>
    {
        var id = medication.nativeEvent.target.selectedIndex;
        localStorage.setItem('medId',id);
        console.log(this.state.medications[id]);
        if(! (medication.nativeEvent.target[id].text === "--"))
            this.setState({
                medication: this.state.medications[id-1],
                selectedMedication: true
                }
            )
        else
            this.setState({
                    selectedMedication: false
                }
            )
    };

    handleSubmitMedicationPrescription(){
        let medicationPrescription={
            medicationPlan:{
                id: this.state.planId
            },
            medication: this.state.medication,
            intake:this.state.formControls.intake.value
        }

        this.setState({
            medicationAdded:true
        });
        console.log("MedicationPrescription"+medicationPrescription);
        //if(!(localStorage.getItem('planName')===''))
        this.registerMedicationIntoPlan(medicationPrescription);
    }


    render() {
        console.log("Medication plan FORM, plan id:"+this.state.planId);
        console.log(this.state.start)
        return (
            <div>
                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='periodStart'>
                    <Label for='periodStartField'> Period start: </Label>
                    <DatePicker name='periodStart' id='periodStartField'
                                onChange={this.handleChangeDateStart}
                                value={this.state.startDate}
                                defaultValue={this.state.formControls.periodStart.value}
                                touched={this.state.formControls.periodStart.touched? 1 : 0}
                                minDate={new Date()}
                                clearIcon={null}
                                readOnly={true}
                                dateFormat="dd/MM/YYYY"
                                required

                    />
                    {this.state.formControls.periodStart.touched && !this.state.formControls.periodStart.valid &&
                    <div className={"error-message"}> * periodStart must be after before today!</div>}
                </FormGroup>

                <FormGroup id='periodStop'>
                    <Label for='periodStopField'> Period stop: </Label>
                    <DatePicker name='periodStop' id='periodStopField'
                                onChange={this.handleChangeDateStop}
                                value={this.state.stopDate}
                                defaultValue={this.state.formControls.periodStop.value}
                                touched={this.state.formControls.periodStop.touched? 1 : 0}
                                minDate={addDays(new Date(), +1)}
                                clearIcon={null}
                                readOnly={true}
                                dateFormat="dd/MM/YYYY"
                                required

                    />
                    {this.state.formControls.periodStop.touched && !this.state.formControls.periodStop.valid &&
                    <div className={"error-message"}> * Birthdate must be before today!</div>}
                </FormGroup>


                <FormGroup id='patient'>
                    <Label for='patientField'> Patient: </Label>
                    <select name='patient' id='patientField'
                            onChange={this.handleChangePatient}>
                        <option>--</option>
                        {this.state.patients.map( patient=>(
                            <option key={patient.id} value={patient.name}>
                                {patient.name}
                            </option>
                        ))}
                    </select>
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!(this.state.formIsValid || !this.state.formControls.intake.valid)
                        || !this.state.selectedPatient ||!this.state.selecteddate2 || this.state.pressed}
                                onClick={this.handleSubmit}>
                            Create medication plan </Button>
                    </Col>
                </Row>
                {
                    this.state.pressed &&
                   <div>
                        <FormGroup id='medication'>
                            <Label for='medicationField'> Medication: </Label>
                            <select name='medication' id='medicationField'
                            onChange={this.handleChangeMedication}>
                                <option>--</option>
                                {this.state.medications.map( medication=>(
                                <option key={medication.id} value={medication.name}>
                            {medication.name}
                            </option>
                            ))}
                            </select>
                        </FormGroup>

                       <FormGroup id='intake'>
                           <Label for='intakeField'> Intake interval: </Label>
                           <Input name='intake' id='intake' placeholder={this.state.formControls.intake.placeholder}
                                  onChange={this.handleChange}
                                  defaultValue={this.state.formControls.intake.value}
                                  touched={this.state.formControls.intake.touched? 1 : 0}
                                  valid={this.state.formControls.intake.valid}
                                  required
                           />
                           {this.state.formControls.intake.touched && !this.state.formControls.intake.valid &&
                           <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                       </FormGroup>

                        <Row>
                        <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formControls.intake.valid || !this.state.selectedMedication}
                                onClick={this.handleSubmitMedicationPrescription}>
                        Add medication </Button>
                        </Col>
                        </Row>
                   </div>
                }



                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}
export default MedicationPlanForm;

