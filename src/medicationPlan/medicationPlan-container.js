import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Row
} from 'reactstrap';

import * as API_PATIENTS from "../patient/api/patient-api";
import * as API_MEDICATIONS from"../medication/api/medication-api";
import * as API_PLANS from "./medicationPlan-api";
import MedicationPlanForm from "./medicationPlan-form";
import NavigationBar from "../navigation-bar";




class MedicationPlanContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);

        this.state = {

            medications: [],
            patients: [],
            created:false,

            id: '',
            selected: false,
            collapseForm: false,
            collapseFormUpdate:false,

            isLoadedMedications: false,
            isLoadedPatients:false,
            isLoadedPlans:false,
            errorStatus: 0,
            error: null
        }

    }

    componentDidMount() {
        this.fetchMedication();
        this.fetchPatients();
   //     this.fetchMedicationPlans()

    }

    fetchMedication(){
        return API_MEDICATIONS.getMedications((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    medications: result,
                    isLoadedMedications: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchPatients() {
        return API_PATIENTS.getPatients((result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    patients: result,
                    isLoadedPatients: result
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchMedicationPlans() {
        return API_PLANS.getMedicationPlans(3,(result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    plans: result,
                    isLoadedPlans: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }
    setCreated(){
        this.setState({
            created:true
        });
    }

    reload() {
        this.setState({
            isLoaded: false,
            isLoadedPatients:false,
            isLoadedMedications:false
        });
        this.toggleForm();
        this.fetchMedication();
        this.fetchPatients();
    }

    render() {
        //console.log("Creat?"+this.state.created);
        console.log("Plan: "+localStorage.getItem('planName'));
        //console.log(localStorage.getItem('planName')==='');
        //console.log("a doua parte:"+!(localStorage.getItem('planName')===''));
        return (
            <div>
                <NavigationBar />
                <CardHeader>
                    <strong> Create medication plan  </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '10', offset: 1}}>
                            {this.state.isLoadedMedications && this.state.isLoadedPatients  &&
                            <MedicationPlanForm medications = {this.state.medications}
                                                patients={this.state.patients}
                                                reloadHandler={this.reload}
                                                setCreated={this.setCreated}/>}


                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>
            </div>
        )
    }
}


export default MedicationPlanContainer;
