import React from 'react';
import validate from "../patient/components/validators/patient-validators";
import Button from "react-bootstrap/Button";
import * as API_MEDICATIONPLANS from "./medicationPlan-api";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';
import DatePicker from "react-date-picker";
import {addDays} from "date-fns";



class MedicationPlanPrescriptionForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
      //  this.reloadHandler = this.props.reloadHandler;
        this.state = {

            medications:this.props.medications,
            created:true,

            medication:true,
            selectedMedication:false,

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                intake: {
                    value: '',
                    placeholder: 'Hours when the madication should be administrated',
                    valid: false,
                    touched: false
                },
            }
        }
        this.handleChangeMedication = this.handleChangeMedication.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange=this.handleChange.bind(this);
    }



    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }



    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }
        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    registerMedicationIntoPlan(medication) {
        return API_MEDICATIONPLANS.postMedicationPlan(medication, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                this.setState({
                    planId:result,
                    created:true
                })
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }



    handleChangeMedication=medication=>
    {
        var id = medication.nativeEvent.target.selectedIndex;
        if(! (medication.nativeEvent.target[id].text === "--")) {
            this.setState({
                medication: this.state.medications[id],
                selectedMedication:true
            });

        }

    };

    handleSubmit() {
        let medicationPrescription = {
            medicationPlan: this.state.plan,
            medication: this.state.medication,
            intake: this.state.formControls.intake.value
        };
        console.log("Medification prescription"+ medicationPrescription);
        this.registerMedicationIntoPlan(medicationPrescription);
    }

    handleSubmitMedicationPrescription(){
        this.registerMedicationIntoPlan()
    }

    exitPlan(){
        localStorage.setItem('planName','');
        localStorage.setItem('patientName','');
    }


    render() {
        return (
            <div>
                <h4>Insert medication into plan: {localStorage.getItem('planName')+" "}
                 for patient { localStorage.getItem('patientName')}</h4>
                        <FormGroup id='medication'>
                            <Label for='medicationField'> Medication: </Label>
                            <select name='medication' id='medicationField'
                                    onChange={this.handleChangeMedication}>
                                {this.state.medications.map( medication=>(
                                    <option key={medication.id} value={medication.name}>
                                        {medication.name}
                                    </option>
                                ))}
                            </select>
                        </FormGroup>

                        <FormGroup id='intake'>
                            <Label for='intakeField'> Intake interval: </Label>
                            <Input name='intake' id='intake' placeholder={this.state.formControls.intake.placeholder}
                                   onChange={this.handleChange}
                                   defaultValue={this.state.formControls.intake.value}
                                   touched={this.state.formControls.intake.touched? 1 : 0}
                                   valid={this.state.formControls.intake.valid}
                                   required
                            />
                            {this.state.formControls.intake.touched && !this.state.formControls.intake.valid &&
                            <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                        </FormGroup>

                        <Row>
                            <Col sm={{size: '4', offset: 8}}>
                                <Button type={"submit"} disabled={!this.state.formControls.intake.valid } onClick={this.handleSubmitMedicationPrescription}>
                                    Add medication </Button>
                            </Col>
                        </Row>
                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} onClick={
                            this.exitPlan()
                        }>
                            Finish editing plan </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}
export default MedicationPlanPrescriptionForm;

