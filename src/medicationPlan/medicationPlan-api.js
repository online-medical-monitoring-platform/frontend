import {HOST} from '../commons/hosts';
import RestApiClient from "../commons/api/rest-client";


const endpoint = {
    medicationPlan: '/medicationPlan',
    medicationPrescription:'/medicationPrescription'
};

function getMedicationPlans(callback) {
    let request = new Request(HOST.backend_api + endpoint.medicationPlan, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicationPlanById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.medicationPlan + params.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postMedicationPlan(medicationPlan, callback){
    let request = new Request(HOST.backend_api + endpoint.medicationPlan , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medicationPlan)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteMedicationPlan(params,callback){
    let request = new Request(HOST.backend_api + endpoint.medicationPlan +'/'+ params, {
        method: 'DELETE',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicationPrescriptions(id, callback){
    let request = new Request(HOST.backend_api + endpoint.medicationPlan + "/"+id+ endpoint.medicationPrescription, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function updateMedicationPlan(medication,params, callback){
    let request = new Request(HOST.backend_api + endpoint.medicationPlan +'/'+ params, {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medication)
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getMedicationPlans,
    getMedicationPlanById,
    postMedicationPlan,
    deleteMedicationPlan,
    updateMedicationPlan,
    getMedicationPrescriptions
};
