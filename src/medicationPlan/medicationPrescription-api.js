import {HOST} from '../commons/hosts';
import RestApiClient from "../commons/api/rest-client";


const endpoint = {
    medicationPrescription:'/medicationPrescription'
};



function postMedicationPrescription(medicationPrescription, callback){
    let request = new Request(HOST.backend_api + endpoint.medicationPrescription , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medicationPrescription)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


export {
    postMedicationPrescription
};
