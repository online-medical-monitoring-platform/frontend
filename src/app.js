import React from 'react'
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import HomeDoctor from './home/homeDoctor.js';
import HomePatient from './home/homePatient.js';

import PersonContainer from './person/person-container'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import PatientContainer from "./patient/patient-container";
import CaregiverContainer from "./caregiver/caregiver-container";
import MedicationContainer from "./medication/medication-container";
import LoginContainer from "./login/login-container";
import HomeCaregiver from "./home/homeCaregiver";
import PatientPage from "./patient/patient-page";
import MedicationPlanContainer from "./medicationPlan/medicationPlan-container";
import CaregiverPage from "./caregiver/caregiver-page";


class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Redirect to='/login'/>}
                        />

                        <Route
                            exact
                            path='/login'
                            render={() => <LoginContainer/>}
                        />



                        <Route
                            exact
                            path='/patient'
                            render={() =>localStorage.getItem('role') === 'patient' ? (<PatientPage/>)
                                :(
                                    <Redirect to='/login'/>
                                )}
                        />

                        <Route
                            exact
                            path='/caregiver'
                            render={() =>localStorage.getItem('role') === 'caregiver' ? (<CaregiverPage/>)
                                :(
                                    <Redirect to='/login'/>
                                )}
                        />


                        <Route
                            exact
                            path='/doctor/patient'
                            render={() =>localStorage.getItem('role') === 'doctor' ? (<PatientContainer/>)
                                :(
                                    <Redirect to='/login'/>
                                )}
                        />

                        <Route
                            exact
                            path='/doctor'
                            render={() =>localStorage.getItem('role') === 'doctor' ? (<HomeDoctor/>)
                                :(
                                    <Redirect to='/login'/>
                                )}
                        />

                        <Route
                            exact
                            path='/doctor/patient'
                            render={() => <PatientContainer/>}
                        />

                        <Route
                            exact
                            path='/doctor/caregiver'
                            render={() =>localStorage.getItem("role") === "doctor" ? (<CaregiverContainer/>)
                                :(
                                    <Redirect to='/login'/>
                                )}

                        />

                        <Route
                            exact
                            path='/doctor/medication'
                            render={() =>localStorage.getItem('role') === 'doctor' ? (<MedicationContainer/>)
                                :(
                                    <Redirect to='/login'/>
                                )}
                        />

                        <Route
                            exact
                            path='/doctor/medicationPlan'
                            render={() =>localStorage.getItem('role') === 'doctor' ? (<MedicationPlanContainer/>)
                                :(
                                    <Redirect to='/login'/>
                                )}
                        />

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
