import React from 'react';

import BackgroundImg from '../commons/images/doctor.jpg';

import {Button, Container, Jumbotron} from 'reactstrap';
import NavigationBar from "../navigation-bar";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "660px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'black', };

class HomeDoctor extends React.Component {


    render() {

        return (

            <div>
                <NavigationBar />
                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                        <h1 className="display-3" style={textStyle}>Welcome, doctor!</h1>
                    </Container>
                </Jumbotron>

            </div>
        )
    };
}

export default HomeDoctor
