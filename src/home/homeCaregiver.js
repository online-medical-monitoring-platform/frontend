import React from 'react';

import BackgroundImg from '../commons/images/caregiver.png';

import {Button, Container, Jumbotron} from 'reactstrap';

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "660px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'black', };

class HomeCaregiver extends React.Component {


    render() {

        return (

            <div>
                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                        <h1 className="display-3" style={textStyle}>Welcome, caregiver!</h1>
                    </Container>
                </Jumbotron>

            </div>
        )
    };
}

export default HomeCaregiver
